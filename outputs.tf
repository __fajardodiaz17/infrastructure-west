output "vpc_id" {
  value = module.networking.vpc_id
}

output "public_subnet" {
  value = module.networking.public_subnet
}

output "private_subnet" {
  value = module.networking.private_subnet
}

output "public_security_group" {
  value = module.networking.public_security_group
}

output "private_security_group" {
  value = module.networking.private_security_group
}

output "master_instance_public_ip" {
  value = module.compute.public_ip
}

output "slave_instance_private_ip" {
  value = module.compute.private_ip
}
