terraform {
  required_version = "~> 1.3"

  backend "s3" {
    bucket = "npdterraformbackend"
    key    = "west/terraform.state"
    region = "us-east-1"
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~>4.0"
    }
  }
}

provider "aws" {
  region = "us-west-1"
}

module "networking" {
  source        = "./modules/networking"
  allocation_id = "eipalloc-0eb9d0981a44e6e3a"
}

module "compute" {
  source = "./modules/compute"

  master_type               = "t2.micro"
  slave_type                = "t2.micro"
  instance_ami              = "ami-03f6d497fceb40069"
  availability_zone         = "us-west-1a"
  key_name                  = "AutomatizacionDevopsWest"
  environment               = "development"
  public_subnet_id          = module.networking.public_subnet
  private_subnet_id         = module.networking.private_subnet
  public_security_group_id  = [module.networking.public_security_group]
  private_security_group_id = [module.networking.private_security_group]
  master_instance_count     = 1
  slave_instance_count      = 2
}