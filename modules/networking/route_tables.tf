resource "aws_route_table" "infos_private_rt"{
    vpc_id = aws_vpc.infos_vpc.id

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_nat_gateway.infos_nat_gateway.id
    }

    tags = {
        Name        = "NPD Private Route Table"
        Environment = var.environment
    }
}

resource "aws_route_table" "infos_public_rt"{
    vpc_id = aws_vpc.infos_vpc.id

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.infos_internet_gateway.id
    }

    tags = {
        Name        = "NPD Public Route Table"
        Environment = var.environment
    }
}