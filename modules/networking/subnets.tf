resource "aws_subnet" "infos_public_subnet"{
    vpc_id = aws_vpc.infos_vpc.id
    cidr_block        = "11.0.0.0/24"
    map_public_ip_on_launch = true
    availability_zone = var.availability_zone
    tags = {
        Environment = var.environment,
        Name = "NPD Public Subnet"
    }

    depends_on = [ aws_vpc.infos_vpc ]
}

resource "aws_subnet" "infos_private_subnet"{
    vpc_id = aws_vpc.infos_vpc.id
    cidr_block        = "11.0.1.0/24"
    availability_zone = var.availability_zone
    tags = {
        Environment = var.environment,
        Name = "NPD Private Subnet"
    }

    depends_on = [ aws_vpc.infos_vpc ]
}