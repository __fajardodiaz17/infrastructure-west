resource "aws_internet_gateway" "infos_internet_gateway" {
    vpc_id = aws_vpc.infos_vpc.id

    tags = {
        Environment = var.environment,
        Name = "NPD Internet Gateway"
    }
}