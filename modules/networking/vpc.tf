resource "aws_vpc" "infos_vpc" {
    cidr_block = "11.0.0.0/16"
    enable_dns_support = true
    enable_dns_hostnames = true

    tags = {
        Environment = var.environment,
        Name = "NPD VPC"
    }
}