output "public_ip" {
  value       = aws_instance.infos_master_instance.*.public_ip
  description = "public ip"
  depends_on  = [aws_instance.infos_master_instance]
}

output "private_ip" {
  value       = aws_instance.infos_slave_instance.*.private_ip
  description = "private ip"
  depends_on  = [aws_instance.infos_slave_instance]
}
